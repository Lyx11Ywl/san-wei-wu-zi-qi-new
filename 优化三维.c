/*
三维五子棋：双方各执一色棋子交替置子，通过平面非对角线3个方向、平面对角线6个方向、立体对角线4个方向进行输赢的判断
版本记录：
（1）实现了二维五子棋游戏功能 2019.06.02
（2）实现了三维五子棋六个面对角线和三个竖直方向上输赢的判断功能（使用结构体嵌套二维数组） 2019.06.05
（3）实现了三维五子棋四个体对角线方向上输赢的判断功能 2019.06.06
（4）优化了三维五子棋棋盘的显示 2019.06.09
*/
#include <stdio.h>
#include <Windows.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h> 
#define MAXIMUS 10 //定义棋盘大小(10*10个方框)
#define chess_number 10//定义棋盘高度

struct MyStruct_0
{
    char ChessBoard[MAXIMUS][MAXIMUS];
}MyStruct_0[chess_number];

void InitChessboard(int *i,int *j,int *k,int *color)/*棋盘初始化*/
{
	int p,q,r;
	printf("请输入目标棋子的层数(棋盘的层数为10),eg:5: ");
LOOPO:
	scanf("%d",&r);
	
	if(r<1||r>10)
	{
		printf("提示：输入的层数超出棋盘的层数，请重新输入！\n");
		goto LOOPO;
	}

	printf("请输入棋子的位置(棋盘的规模为10*10),eg:2,2: ");
LOOPT:
	scanf("%d,%d", &p,&q);
 
    if(p<1 || p >10 || q<1 || q>10)
    {
    	
        printf("提示：输入的位置超出棋盘的范围，请重新输入！\n");
    	goto LOOPT;
    }
 
    if(2 != MyStruct_0[r-1].ChessBoard[p-1][q-1])
    {
        printf("提示：该位置已经有棋子了，请重新输入!\n");
        goto LOOPO;
    }
 
    (*color)=(*color+1)%2;//获取棋盘棋子值以对应棋盘符号或棋子颜色 
    MyStruct_0[r-1].ChessBoard[p-1][q-1]=*color;//将该位置的值给棋盘
	*k=r;
	*i=p;
	*j=q;
}

void PrintChessBoard(int k)/*画棋盘*/
{
	int i;
	int j;

	for(i=1;i<=10;i++)
        {
            //第1行
            if(i==1)
            {
                //第1列
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==2)
                    printf("┌ ");
 
                //第2-9列
                for(j=2;j<=9;j++)
                {
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                        printf("●");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                        printf("○");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                        printf("┬ ");
                }
 
                //第10列
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                    printf("┐ ");
 
                    printf("\n");
            }
 
            //第2-9行
            if(i<=9 && i>=2)
            {
                //第1列
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==2)
                    printf("├ ");
 
                //第2-9列
                for(j=2;j<=9;j++)
                {
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                        printf("●");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                        printf("○");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                        printf("┼ ");
                }
 
                //第10列
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                    printf("┤ ");
 
                    printf("\n");
            }
 
            //第10行
            if(i==10)
            {
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][0]==2)
                    printf("└ ");
 
                for(j=2;j<=9;j++)
                {
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                        printf("●");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                        printf("○");
                    if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                        printf("┴ ");
                }
 
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==1)
                    printf("●");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==0)
                    printf("○");
                if(MyStruct_0[k-1].ChessBoard[i-1][j-1]==2)
                    printf("┘ ");
 
                    printf("\n");
            }
 
        }
}

int CheckAcross(int i, int jTemp,int k,int countTemp,int colorFlag) 
{
	while((++jTemp<10 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[k].ChessBoard[i][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckVertical(int j, int iTemp,int k,int countTemp,int colorFlag)
{
	while((++iTemp<10 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[k].ChessBoard[iTemp][j])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckDirect(int j, int i,int kTemp,int countTemp,int colorFlag)
{
	while((++kTemp<10 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[i][j])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckRtLb(int iTemp,int jTemp,int k,int countTemp,int colorFlag)
{
	while((++iTemp<10 )&& (--jTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[k].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckLtRb(int iTemp,int jTemp,int k,int countTemp,int colorFlag)
{
	while((++iTemp<10 )&& (++jTemp<10 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[k].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckRtLb1(int i,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--jTemp>=0 )&& (--kTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[i][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckLtRb1(int i,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (++jTemp<10 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[i][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckRtLb2(int iTemp,int j,int kTemp,int countTemp,int colorFlag)
{
	while((++iTemp<10 )&& (--kTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][j])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckLtRb2(int iTemp,int j,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (--iTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][j])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckBody1(int iTemp,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (--iTemp>=0 )&& (--jTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckBody2(int iTemp,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (--iTemp>=0 )&& (++jTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckBody3(int iTemp,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (++iTemp>=0 )&& (--jTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int CheckBody4(int iTemp,int jTemp,int kTemp,int countTemp,int colorFlag)
{
	while((--kTemp>=0 )&& (++iTemp>=0 )&& (++jTemp>=0 )&&( 5 != countTemp ))
    {
        if(colorFlag == MyStruct_0[kTemp].ChessBoard[iTemp][jTemp])
        {
            countTemp++;
            if(5 == countTemp)
            {
                if(0 == colorFlag)
                {
                    printf("黑棋赢了!\n");
                }
                else if(1 == colorFlag)
                {
                   	printf("白棋赢了!\n");
                }
                return 1;
            }
        }
        else
        {
             countTemp = 0;
             break;
        }
	}
	return 0;
}

int main()
{
	int i,j,k;//表示棋盘横纵坐标
    /*
    *绘制表格需要的字符:┌ ┬ ┐├ ┼ ┤└ ┴ ┘│─●○
    *数组的值0表示黑棋，1表示白棋，2表示该位置没有棋
    */
    int color=0;//0表示黑棋(圆圈)，1表示白棋
    int iTemp = 0,jTemp = 0,kTemp=0,countTemp = 0;
    int colorFlag = 0;
    char ReS;

		
again:
 
	for(k=0;k<10;k++)
		for(i=0;i<10;i++)
			for(j=0;j<10;j++)
				MyStruct_0[k].ChessBoard[i][j]=2;
		

	while(1)
    {       
    	InitChessboard(&i,&j,&k,&color);
    	PrintChessBoard(k);
		//判断输赢
		for(k=0;k<10;k++)
		{
			for(i=0;i<10;i++)
			{
				for(j=0;j<10;j++)
				{
					//count = 0;
					//如果检测到该有棋子，则检查与该棋子有关的是否可以赢
					if(2 != MyStruct_0[k].ChessBoard[i][j])
					{
						colorFlag = MyStruct_0[k].ChessBoard[i][j];
						countTemp = 1;
						iTemp = i;
						jTemp = j;
						kTemp = k;

						if(CheckAcross(i,jTemp,k,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}
                    
						if(CheckVertical(j,iTemp,k,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}
                    
						if(CheckDirect(j,i,kTemp,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}

						if(CheckRtLb(iTemp,jTemp,k,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}
                    
						if(CheckLtRb(iTemp,jTemp,k,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckRtLb1(i,jTemp,kTemp,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}

						if(CheckLtRb1(i,jTemp,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckRtLb2(iTemp,j,kTemp,countTemp,colorFlag))
						{
                    		goto whileEnd;
						}

						if(CheckLtRb2(iTemp,j,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckBody1(iTemp,jTemp,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckBody2(iTemp,jTemp,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckBody3(iTemp,jTemp,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}

						if(CheckBody4(iTemp,jTemp,kTemp,countTemp,colorFlag))
						{
							goto whileEnd;
						}
					}
				}
			}
		}
	}
		
whileEnd:
    printf("重新开始/退出 重新开始请按r/R,退出请按任意键:");
    fflush(stdin);
    ReS = getchar();
 
    if(('r' == ReS) ||( 'R' == ReS))
    {
        system("cls");
        printf("已经重新开始了，请输入第一颗棋子的坐标:\n\n");
        goto again;
    }
}

 


